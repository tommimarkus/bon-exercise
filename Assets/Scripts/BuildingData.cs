﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class BuildingData : MonoBehaviour
{
    public string Name;
    public string Location;
    public int Value;
    public List<BuildingAttribute> Attributes = new List<BuildingAttribute>();
}
