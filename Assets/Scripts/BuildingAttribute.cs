﻿using System;
using UnityEngine;

[Serializable]
public sealed class BuildingAttribute
{
    public Sprite Icon;
    public string Name;
    public string Value;
}