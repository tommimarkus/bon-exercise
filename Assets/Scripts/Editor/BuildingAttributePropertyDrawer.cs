﻿using UnityEditor;
using UnityEngine;

[CustomPropertyDrawer(typeof(BuildingAttribute))]
public class BuildingAttributePropertyDrawer : PropertyDrawer
{
	private const float _elementHeight = 16f;
	private const float _contentHeight = _elementHeight + 2f;
	
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		EditorGUI.BeginProperty(position, label, property);

		position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);
		{
			var indent = EditorGUI.indentLevel;
			EditorGUI.indentLevel = 0;

			var iconRect = new Rect(position.x, position.y, position.width, _elementHeight);
			var nameRect = new Rect(position.x, position.y + _contentHeight, position.width, _elementHeight);
			var valueRect = new Rect(position.x, position.y + _contentHeight * 2, position.width, _elementHeight);

			var labelWidth = EditorGUIUtility.labelWidth;
			EditorGUIUtility.labelWidth = 60;
			EditorGUI.PropertyField(iconRect, property.FindPropertyRelative("Icon"), new GUIContent("Icon"));
			EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("Name"), new GUIContent("Name"));
			EditorGUI.PropertyField(valueRect, property.FindPropertyRelative("Value"), new GUIContent("Value"));
			EditorGUIUtility.labelWidth = labelWidth;

			EditorGUI.indentLevel = indent;
		}
		EditorGUI.EndProperty();
	}

	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		return _contentHeight * 3;
	}
}